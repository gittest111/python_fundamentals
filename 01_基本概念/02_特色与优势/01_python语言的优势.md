# 01_python语言的优势

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. 标准库](#1-标准库)
    - [1.1. 文本处理](#11-文本处理)
    - [1.2. 文件处理](#12-文件处理)
    - [1.3. 操作系统功能](#13-操作系统功能)
    - [1.4. 网络通信](#14-网络通信)
    - [1.5. 网络协议](#15-网络协议)
    - [1.6. W3C 格式支持](#16-w3c-格式支持)
    - [1.7. 其它功能](#17-其它功能)
- [2. 第三方库](#2-第三方库)
    - [2.1. Web 框架](#21-web-框架)
        - [2.1.1. Django](#211-django)
        - [2.1.2. Flask](#212-flask)
        - [2.1.3. Pyramid](#213-pyramid)
        - [2.1.4. ActiveGrid](#214-activegrid)
        - [2.1.5. Karrigell](#215-karrigell)
        - [2.1.6. Tornado](#216-tornado)
        - [2.1.7. webpy](#217-webpy)
        - [2.1.8. CherryPy](#218-cherrypy)
        - [2.1.9. Pylons](#219-pylons)
        - [2.1.10. Zope](#2110-zope)
        - [2.1.11. TurboGears](#2111-turbogears)
        - [2.1.12. Twisted](#2112-twisted)
        - [2.1.13. Quixote](#2113-quixote)
    - [2.2. 科学计算](#22-科学计算)
        - [2.2.1. Matplotlib](#221-matplotlib)
        - [2.2.2. Pandas](#222-pandas)
        - [2.2.3. SciPy](#223-scipy)
        - [2.2.4. NumPy](#224-numpy)
    - [2.3. GUI](#23-gui)
        - [2.3.1. PyGtk](#231-pygtk)
        - [2.3.2. PyQt](#232-pyqt)
        - [2.3.3. WxPython](#233-wxpython)
    - [2.4. 其它](#24-其它)
        - [2.4.1. BeautifulSoup](#241-beautifulsoup)
        - [2.4.2. gevent](#242-gevent)
        - [2.4.3. PIL](#243-pil)
        - [2.4.4. PyGame](#244-pygame)
        - [2.4.5. Py2exe](#245-py2exe)
        - [2.4.6. Requests](#246-requests)
        - [2.4.7. scikit-learn](#247-scikit-learn)
        - [2.4.8. TensorFlow](#248-tensorflow)
        - [2.4.9. Keras](#249-keras)
        - [2.4.10. SQLAlchemy](#2410-sqlalchemy)

<!-- /TOC -->

---

<a id="markdown-1-标准库" name="1-标准库"></a>
## 1. 标准库

[基维百科](https://zh.wikipedia.org/wiki/Python#标准库)

<a id="markdown-11-文本处理" name="11-文本处理"></a>
### 1.1. 文本处理

- 文本格式化
- 正则表达式匹配
- 文本差异计算与合并
- Unicode 支持
- 二进制数据处理

<a id="markdown-12-文件处理" name="12-文件处理"></a>
### 1.2. 文件处理

- 文件操作
- 创建临时文件
- 文件压缩与归档
- 操作配置文件

<a id="markdown-13-操作系统功能" name="13-操作系统功能"></a>
### 1.3. 操作系统功能

- 线程与进程支持
- IO 复用
- 日期与时间处理
- 调用系统函数
- 日志（logging）

<a id="markdown-14-网络通信" name="14-网络通信"></a>
### 1.4. 网络通信

- 网络套接字
- SSL 加密通信
- 异步网络通信

<a id="markdown-15-网络协议" name="15-网络协议"></a>
### 1.5. 网络协议

- HTTP
- FTP
- SMTP
- POP
- IMAP
- NNTP
- XMLRPC

<a id="markdown-16-w3c-格式支持" name="16-w3c-格式支持"></a>
### 1.6. W3C 格式支持

- HTML
- SGML
- XML
- JSON

<a id="markdown-17-其它功能" name="17-其它功能"></a>
### 1.7. 其它功能

- 国际化支持
- 数学运算
- HASH
- Tkinter

<a id="markdown-2-第三方库" name="2-第三方库"></a>
## 2. 第三方库

[基维百科](https://zh.wikipedia.org/wiki/Python#著名第三方库)

<a id="markdown-21-web-框架" name="21-web-框架"></a>
### 2.1. Web 框架

<a id="markdown-211-django" name="211-django"></a>
#### 2.1.1. Django

开源Web开发框架，它鼓励快速开发,并遵循MVC设计，开发周期短

<a id="markdown-212-flask" name="212-flask"></a>
#### 2.1.2. Flask

轻量级的Web框架

<a id="markdown-213-pyramid" name="213-pyramid"></a>
#### 2.1.3. Pyramid

轻量，同时有可以规模化的Web框架，Pylon projects 的一部分

<a id="markdown-214-activegrid" name="214-activegrid"></a>
#### 2.1.4. ActiveGrid

企业级的Web2.0解决方案

<a id="markdown-215-karrigell" name="215-karrigell"></a>
#### 2.1.5. Karrigell

简单的Web框架，自身包含了Web服务，py脚本引擎和纯python的数据库PyDBLite
<a id="markdown-216-tornado" name="216-tornado"></a>
#### 2.1.6. Tornado

一个轻量级的Web框架，内置非阻塞式服务器，而且速度相当快

<a id="markdown-217-webpy" name="217-webpy"></a>
#### 2.1.7. webpy

一个小巧灵活的Web框架，虽然简单但是功能强大

<a id="markdown-218-cherrypy" name="218-cherrypy"></a>
#### 2.1.8. CherryPy

基于Python的Web应用程序开发框架

<a id="markdown-219-pylons" name="219-pylons"></a>
#### 2.1.9. Pylons

基于Python的一个极其高效和可靠的Web开发框架

<a id="markdown-2110-zope" name="2110-zope"></a>
#### 2.1.10. Zope

开源的Web应用服务器

<a id="markdown-2111-turbogears" name="2111-turbogears"></a>
#### 2.1.11. TurboGears

基于Python的MVC风格的Web应用程序框架

<a id="markdown-2112-twisted" name="2112-twisted"></a>
#### 2.1.12. Twisted

流行的网络编程库，大型Web框架

<a id="markdown-2113-quixote" name="2113-quixote"></a>
#### 2.1.13. Quixote

Web开发框架

<a id="markdown-22-科学计算" name="22-科学计算"></a>
### 2.2. 科学计算

<a id="markdown-221-matplotlib" name="221-matplotlib"></a>
#### 2.2.1. Matplotlib

用Python实现的类matlab的第三方库，用以绘制一些高质量的数学二维图形

<a id="markdown-222-pandas" name="222-pandas"></a>
#### 2.2.2. Pandas

用于数据分析、数据建模、数据可视化的第三方库

<a id="markdown-223-scipy" name="223-scipy"></a>
#### 2.2.3. SciPy

基于Python的matlab实现，旨在实现matlab的所有功能

<a id="markdown-224-numpy" name="224-numpy"></a>
#### 2.2.4. NumPy

基于Python的科学计算第三方库，提供了矩阵，线性代数，傅立叶变换等等的解决方案

<a id="markdown-23-gui" name="23-gui"></a>
### 2.3. GUI

<a id="markdown-231-pygtk" name="231-pygtk"></a>
#### 2.3.1. PyGtk

基于Python的GUI程序开发GTK+库

<a id="markdown-232-pyqt" name="232-pyqt"></a>
#### 2.3.2. PyQt

用于Python的QT开发库

<a id="markdown-233-wxpython" name="233-wxpython"></a>
#### 2.3.3. WxPython

Python下的GUI编程框架，与MFC的架构相似

<a id="markdown-24-其它" name="24-其它"></a>
### 2.4. 其它

<a id="markdown-241-beautifulsoup" name="241-beautifulsoup"></a>
#### 2.4.1. BeautifulSoup

基于Python的HTML/XML解析器，简单易用。

<a id="markdown-242-gevent" name="242-gevent"></a>
#### 2.4.2. gevent

python的一个高性能并发框架,使用了epoll事件监听、协程等机制将异步调用封装为同步调用。

<a id="markdown-243-pil" name="243-pil"></a>
#### 2.4.3. PIL

基于Python的图像处理库，功能强大，对图形文件的格式支持广泛。目前已无维护，另一个第三方库Pillow实现了对PIL库的支持和维护。

<a id="markdown-244-pygame" name="244-pygame"></a>
#### 2.4.4. PyGame

基于Python的多媒体开发和游戏软件开发模块。

<a id="markdown-245-py2exe" name="245-py2exe"></a>
#### 2.4.5. Py2exe

将python脚本转换为windows上可以独立运行的可执行程序。

<a id="markdown-246-requests" name="246-requests"></a>
#### 2.4.6. Requests

适合于人类使用的HTTP库，封装了许多繁琐的HTTP功能，极大地简化了HTTP请求所需要的代码量。

<a id="markdown-247-scikit-learn" name="247-scikit-learn"></a>
#### 2.4.7. scikit-learn

机器学习第三方库，实现许多知名的机器学习算法。

<a id="markdown-248-tensorflow" name="248-tensorflow"></a>
#### 2.4.8. TensorFlow

Google开发维护的开源机器学习库。

<a id="markdown-249-keras" name="249-keras"></a>
#### 2.4.9. Keras

基于TensorFlow，Theano与CNTK的高阶神经网络API。

<a id="markdown-2410-sqlalchemy" name="2410-sqlalchemy"></a>
#### 2.4.10. SQLAlchemy

关系型数据库的对象关系映射(ORM)工具
