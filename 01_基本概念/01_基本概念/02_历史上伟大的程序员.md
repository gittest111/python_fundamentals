# 02_历史上伟大的程序员

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. 第一位计算机程序员：埃达·洛夫莱斯 Ada Lovelace](#1-第一位计算机程序员埃达·洛夫莱斯-ada-lovelace)
- [2. Pascal之父：尼克劳斯·维尔特 Niklaus Wirth](#2-pascal之父尼克劳斯·维尔特-niklaus-wirth)
- [3. 微软创始人：比尔·盖茨 Bill Gates](#3-微软创始人比尔·盖茨-bill-gates)
- [4. Java之父：詹姆斯·高斯林 James Gosling](#4-java之父詹姆斯·高斯林-james-gosling)
- [5. Python之父：吉多·范罗苏姆 Guido van Rossum](#5-python之父吉多·范罗苏姆-guido-van-rossum)
- [6. B语言、C语言和Unix创始人：肯·汤普逊 Ken Thompson](#6-b语言c语言和unix创始人肯·汤普逊-ken-thompson)
- [7. 现代计算机科学先驱：高德纳 Donald Knuth](#7-现代计算机科学先驱高德纳-donald-knuth)
- [8. 《C程序设计语言》的作者：布莱恩·柯林汉 Brian Kernighan](#8-c程序设计语言的作者布莱恩·柯林汉-brian-kernighan)
- [9. 互联网之父：蒂姆·伯纳斯-李 Tim Berners-Lee](#9-互联网之父蒂姆·伯纳斯-李-tim-berners-lee)
- [10. C++之父：比雅尼·斯特劳斯特鲁普 Bjarne Stroustrup](#10-c之父比雅尼·斯特劳斯特鲁普-bjarne-stroustrup)
- [11. Linux之父：林纳斯·托瓦兹 Linus Torvalds](#11-linux之父林纳斯·托瓦兹-linus-torvalds)
- [12. C语言和Unix之父：丹尼斯·里奇 Dennis Ritchie](#12-c语言和unix之父丹尼斯·里奇-dennis-ritchie)
- [13. C#之父 安德斯·海尔斯伯格 Anders Hejlsberg](#13-c之父-安德斯·海尔斯伯格-anders-hejlsberg)

<!-- /TOC -->

---

所谓程序员，是指那些能够创造、编写计算机程序的人。

不论一个人是什么样的程序员，或多或少，他都在为我们这个社会贡献着什么东西。

然而，有些程序员的贡献却超过了一个普通人一辈子能奉献的力量。

这些程序员是先驱，受人尊重，他们贡献的东西改变了我们人类的整个文明进程。

下面就让我们看看人类历史上那些伟大的程序员。

- [ ] <font color='red'>欢迎大家补充更新</font>

<a id="markdown-1-第一位计算机程序员埃达·洛夫莱斯-ada-lovelace" name="1-第一位计算机程序员埃达·洛夫莱斯-ada-lovelace"></a>
## 1. 第一位计算机程序员：埃达·洛夫莱斯 Ada Lovelace

[基维百科](https://zh.wikipedia.org/wiki/愛達·勒芙蕾絲)
[百度百科](https://baike.baidu.com/item/阿达·洛芙莱斯?fromtitle=Ada+Lovelace&fromid=6825878)

![Ada Lovelace](https://gitee.com/uploads/images/2018/0426/181029_7e87719a_675903.jpeg "Ada Lovelace.jpg")

埃达·洛夫莱斯，原名 奥古斯塔·埃达·拜伦，是著名英国诗人拜伦之女。

她是数学爱好者，被后人公认为 **第一位计算机程序员**。

在1842年与1843年期间，埃达花了9个月的时间，翻译意大利数学家 路易吉·米那比亚 讲述 查尔斯·巴贝奇 计算机分析机的论文。

在译文后面，她增加了许多注记，详细说明用该机器计算伯努利数的方法。

后来被认为是世界上第一个计算机程序，因此，埃达也被认为是世界上第一位程序员。

不过，有传记作者也因为部份的程序是由巴贝奇本人所撰，而质疑埃达的原创性。

埃达的文章创造出许多巴贝奇也未曾提到的新构想，比如埃达曾经预言道：

> 这个机器未来可以用来排版、编曲或是各种更复杂的用途。

1852年，埃达为了治疗子宫颈癌，却因此死于失血过多，年仅36岁。

她死后一百年，之前对查尔斯·巴贝奇的《分析机概论》所留下的笔记被重新公布，并被认为对现代计算机与软件工程造成了重大影响。

<a id="markdown-2-pascal之父尼克劳斯·维尔特-niklaus-wirth" name="2-pascal之父尼克劳斯·维尔特-niklaus-wirth"></a>
## 2. Pascal之父：尼克劳斯·维尔特 Niklaus Wirth

[基维百科](https://zh.wikipedia.org/wiki/尼克劳斯·维尔特)
[百度百科](https://baike.baidu.com/item/尼古拉斯·沃斯?fromtitle=Niklaus+Wirth&fromid=3047962)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115549_3f111256_675903.jpeg "Niklaus Wirth.jpg")

尼克劳斯·埃米尔·维尔特，生于瑞士温特图尔，是瑞士计算机科学家。

1963年到1967年，他成为斯坦福大学的计算机科学部助理教授，之后在苏黎世大学担当相同的职位。

1968年，他成为苏黎世联邦理工学院的信息学教授，又往施乐帕洛阿尔托研究中心进修了两年。

他是好几种编程语言的主设计师，包括 Algol W，Modula，Pascal，Modula-2，Oberon等。

他亦是Euler语言的发明者之一。1984年他因发展了这些语言而获图灵奖。

他亦是Lilith电脑和Oberon系统的设计和运行队伍的重要成员。

他的文章 Program Development by Stepwise Refinement 视为软件工程中的经典之作。

他写的一本书的书名《**算法 + 数据结构 = 程序**》是计算机科学的名句。

<a id="markdown-3-微软创始人比尔·盖茨-bill-gates" name="3-微软创始人比尔·盖茨-bill-gates"></a>
## 3. 微软创始人：比尔·盖茨 Bill Gates

[基维百科](https://zh.wikipedia.org/wiki/比尔·盖茨)
[百度百科](https://baike.baidu.com/item/比尔·盖茨/83241?fromtitle=Bill+Gates&fromid=414056)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115558_994ef846_675903.jpeg "Bill Gates.jpg")

威廉·亨利·"比尔"·盖茨三世，是一名美国著名企业家、投资者、软件工程师、慈善家。

早年，他与保罗·艾伦一起创建了微软公司，曾任微软董事长、CEO和首席软件设计师。

<a id="markdown-4-java之父詹姆斯·高斯林-james-gosling" name="4-java之父詹姆斯·高斯林-james-gosling"></a>
## 4. Java之父：詹姆斯·高斯林 James Gosling

[基维百科](https://zh.wikipedia.org/wiki/詹姆斯·高斯林)
[百度百科](https://baike.baidu.com/item/詹姆斯·高斯林?fromtitle=James+Gosling&fromid=4616494)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115607_23b38082_675903.jpeg "James Gosling.jpg")

一般公认他为 **Java之父**。

詹姆斯·高斯林，出生于加拿大，软件专家，Java编程语言的共同创始人之一。

在他12岁的时候，他已能设计电子游戏机，帮忙邻居修理收割机。

大学时期在天文系担任程式开发工读生，

1977年获得了加拿大卡尔加里大学计算机科学学士学位。

1981年开发在Unix上运行的Emacs类编辑器Gosling Emacs。

1983年获得了美国卡内基梅隆大学计算机科学博士学位，

博士论文的题目是《The Algebraic Manipulation of Constraints》。

毕业后到 IBM 工作，设计IBM第一代工作站NeWS系统，后来转至Sun公司。

1990年，与 Patrick Naughton 和 Mike Sheridan 等人合作“绿色计划”, 后来发展一套语言叫做 Oak，后改名为 **Java**。

1994年底，James Gosling在硅谷召开的“技术、教育和设计大会”上展示Java程式。

2000年，Java成为世界上最流行的电脑语言。

<a id="markdown-5-python之父吉多·范罗苏姆-guido-van-rossum" name="5-python之父吉多·范罗苏姆-guido-van-rossum"></a>
## 5. Python之父：吉多·范罗苏姆 Guido van Rossum

[基维百科](https://zh.wikipedia.org/wiki/吉多·范罗苏姆)
[百度百科](https://baike.baidu.com/item/Guido%20van%20Rossum)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115617_8a477b6d_675903.jpeg "Guido van Rossum.jpg")

吉多·范罗苏姆是一名荷兰计算机程序员，他作为Python程序设计语言的作者而为人们熟知。

在Python社区，吉多·范罗苏姆被人们认为是“仁慈的独裁者（BDFL）”，意思是他仍然关注Python的开发进程，并在必要的时刻做出决定。

2002年，在比利时布鲁塞尔举办的自由及开源软件开发者欧洲会议上，吉多·范罗苏姆获得了由自由软件基金会颁发的2001年自由软件进步奖。

2003年五月，吉多获得了荷兰UNIX用户小组奖。

2006年，他被美国计算机协会（ACM）认定为著名工程师。

<a id="markdown-6-b语言c语言和unix创始人肯·汤普逊-ken-thompson" name="6-b语言c语言和unix创始人肯·汤普逊-ken-thompson"></a>
## 6. B语言、C语言和Unix创始人：肯·汤普逊 Ken Thompson

[基维百科](https://zh.wikipedia.org/wiki/肯·汤普逊)
[百度百科](https://baike.baidu.com/item/Ken%20Thompson)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115627_80494d3c_675903.jpeg "Ken Thompson.jpg")

肯尼斯·蓝·汤普逊，小名为肯·汤普逊，生于美国新奥尔良，计算机科学学者与软件工程师。

他与丹尼斯·里奇设计了B语言、C语言，创建了Unix和Plan 9操作系统，他也是编程语言Go的共同作者。与丹尼斯·里奇同为 **1983年图灵奖得主**。

肯·汤普逊的贡献还包括了发展正规表示法，写作了早期的电脑文字编辑器QED与ed，定义UTF-8编码，以及发展电脑象棋。

<a id="markdown-7-现代计算机科学先驱高德纳-donald-knuth" name="7-现代计算机科学先驱高德纳-donald-knuth"></a>
## 7. 现代计算机科学先驱：高德纳 Donald Knuth

[基维百科](https://zh.wikipedia.org/wiki/高德纳)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115639_6ed893bf_675903.jpeg "Donald Knuth.jpg")

唐纳德·尔文·克努斯，出生于美国密尔沃基，著名计算机科学家，斯坦福大学计算机系荣誉退休教授。

高德纳教授为现代计算机科学的先驱人物，创造了算法分析的领域，在数个理论计算机科学的分支做出基石一般的贡献。

在计算机科学及数学领域发表了多部具广泛影响的论文和著作。

1974年图灵奖得主。

高德纳最为人知的事迹是，他是《计算机程序设计艺术》的作者。

此书是计算机科学界最受高度敬重的参考书籍之一。

此外还是排版软件TEX和字体设计系统Metafont的发明人，提出文学编程的概念，并创造了WEB与CWEB软件，作为文学编程开发工具。

<a id="markdown-8-c程序设计语言的作者布莱恩·柯林汉-brian-kernighan" name="8-c程序设计语言的作者布莱恩·柯林汉-brian-kernighan"></a>
## 8. 《C程序设计语言》的作者：布莱恩·柯林汉 Brian Kernighan

[基维百科](https://zh.wikipedia.org/wiki/布萊恩·柯林漢)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115649_d51ebb86_675903.jpeg "Bjarne Stroustrup.jpg")

布莱恩·威尔森·柯林汉，生于加拿大多伦多，加拿大计算机科学家，曾服务于贝尔实验室，为普林斯顿大学教授。

他曾参与Unix的研发，也是AMPL与AWK的共同创造者之一。

与丹尼斯·里奇共同写作了C语言的第一本著作《C程序设计语言》。

他也创作了许多Unix上的程式，包括在Version 7 Unix上的 ditroff 与 cron。

<a id="markdown-9-互联网之父蒂姆·伯纳斯-李-tim-berners-lee" name="9-互联网之父蒂姆·伯纳斯-李-tim-berners-lee"></a>
## 9. 互联网之父：蒂姆·伯纳斯-李 Tim Berners-Lee

[基维百科](https://zh.wikipedia.org/wiki/蒂姆·伯纳斯-李)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115658_9f7320b8_675903.jpeg "Tim Berners-Lee.jpg")

蒂莫西·约翰·伯纳斯-李爵士，昵称为蒂姆·伯纳斯-李（Tim Berners-Lee），英国计算机科学家。

他是万维网的发明者，麻省理工学院教授。

1990年12月25日，罗伯特·卡里奥成功通过Internet实现了HTTP代理与服务器的第一次通讯。

伯纳斯-李为关注万维网发展而创办的组织，万维网联盟的主席。

他也是万维网基金会的创办人。

伯纳斯-李还是麻省理工学院计算机科学及人工智能实验室创办主席及高级研究员。

同时，伯纳斯-李是网页科学研究倡议会的总监。

最后，他是麻省理工学院集体智能中心咨询委员会成员。

2004年，英女皇伊丽莎白二世向伯纳斯-李颁发大英帝国爵级司令勋章。

2009年4月，他获选为美国国家科学院外籍院士。

在2012年夏季奥林匹克运动会开幕典礼上，他获得了“万维网发明者”的美誉。

伯纳斯-李本人也参与了开幕典礼，在一台NeXT计算机前工作。

他在Twitter上发表消息说：“这是给所有人的”，体育馆内的LCD光管随即显示出文字来。

<a id="markdown-10-c之父比雅尼·斯特劳斯特鲁普-bjarne-stroustrup" name="10-c之父比雅尼·斯特劳斯特鲁普-bjarne-stroustrup"></a>
## 10. C++之父：比雅尼·斯特劳斯特鲁普 Bjarne Stroustrup

[基维百科](https://zh.wikipedia.org/wiki/比雅尼·斯特劳斯特鲁普)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115711_890bdb84_675903.jpeg "Bjarne Stroustrup.jpg")

比雅尼·斯特劳斯特鲁普，生于丹麦奥胡斯郡，计算机科学家，德州农工大学工程学院的计算机科学首席教授。

他以创造C++编程语言而闻名，被称为“C++之父”。

他还写了一本《C++程序设计语言》，它被许多人认为是 C++ 的范本经典，目前是第四版（于2013年5月19日出版），最新版中囊括了C++11所引进的一些新特性。

<a id="markdown-11-linux之父林纳斯·托瓦兹-linus-torvalds" name="11-linux之父林纳斯·托瓦兹-linus-torvalds"></a>
## 11. Linux之父：林纳斯·托瓦兹 Linus Torvalds

[基维百科](https://zh.wikipedia.org/wiki/林纳斯·托瓦兹)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115722_3cba44a3_675903.jpeg "Linus Torvalds.jpg")

林纳斯·本纳第克特·托瓦兹，生于芬兰赫尔辛基市，拥有美国国籍。

他是Linux内核的最早作者，随后发起了这个开源项目，担任Linux内核的首要架构师与项目协调者。

是当今世界最著名的电脑程序员、黑客之一。

他还发起了Git这个开源项目，并为主要的开发者。

林纳斯在网上邮件列表中也以火暴的脾气著称。

例如，有一次与人争论Git为何不使用C++开发时与对方用“放屁”互骂。

他更曾以“一群自慰的猴子” 来称呼OpenBSD团队。

2012年6月14日，托瓦兹在出席芬兰的阿尔托大学所主办的一次活动时称，Nvidia是他所接触过的“最烂的公司”( the worst company）和 “最麻烦的公司”，因为Nvidia一直没有针对Linux平台发布任何官方的Optimus支持，随后托瓦兹当众对着镜头竖起了中指，说“ Nvidia，操你的! ”（So, Nvidia, fuck you!）。

<a id="markdown-12-c语言和unix之父丹尼斯·里奇-dennis-ritchie" name="12-c语言和unix之父丹尼斯·里奇-dennis-ritchie"></a>
## 12. C语言和Unix之父：丹尼斯·里奇 Dennis Ritchie

[基维百科](https://zh.wikipedia.org/wiki/丹尼斯·里奇)

![输入图片说明](https://gitee.com/uploads/images/2018/0425/115732_cc4318b6_675903.jpeg "Dennis Ritchie.jpg")

丹尼斯·麦卡利斯泰尔·里奇，生于美国纽约州布朗克斯维尔（Bronxville），著名的美国计算机科学家。

对C语言和其他编程语言、Multics和Unix等操作系统的发展做出了巨大贡献。

在技术讨论中，他常被称为dmr，这是他在贝尔实验室的用户名称（username）。

丹尼斯·里奇与肯·汤普逊两人开发了C语言，并随后以之开发出了Unix操作系统，而C语言和Unix在电脑工业史上都占有重要的地位。

C语言至今在开发软件和操作系统时依然是非常常用，且它对许多现代的编程语言（如C++、C#、Java和JavaScript）也有着重大影响。

而在操作系统方面Unix也影响深远，今天市场上有许多操作系统是基于Unix衍生而来（如 AIX 与 System V 等），

同时也有不少系统（通称类 Unix 系统）借鉴了 Unix 的设计思想。

<a id="markdown-13-c之父-安德斯·海尔斯伯格-anders-hejlsberg" name="13-c之父-安德斯·海尔斯伯格-anders-hejlsberg"></a>
## 13. C#之父 安德斯·海尔斯伯格 Anders Hejlsberg

![输入图片说明](https://gitee.com/uploads/images/2018/0425/121948_b3f85410_675903.jpeg "Anders Hejlsberg.jpg")

丹麦人，Borland Turbo Pascal编译器的主要作者。

进入微软公司后，先后主持了Visual J++、.Net， C# 和 TypeScript。

出生于哥本哈根，安德斯·海尔斯伯格曾在丹麦技术大学学习工程，但没有毕业

大学时期他曾替 Nascom microcomputer 撰写程式，他曾为Nascom-2电脑撰写蓝标签（Blue Label）Pascal compiler，
到了DOS时代他又重新改写这套compiler。

当时他在丹麦拥有个叫Poly Data的公司，他编写了Compass Pascal编译器核心，后来叫Poly Pascal。

1986年他首次认识了Philippe Kahn（Borland的创立者）。

1996年Hejlsberg加入微软公司。

据说，比尔·盖茨亲自参与了这次挖角行动，年薪三百万美元，并许诺安德斯·海尔斯伯格在微软将得到技术上的足够自由和资源支持。

据说该事件也是微软公司和Borland公司后续官司的导火索。

进入微软公司后，首先主持了Visual J++的开发工作，由于在Java开发工具授权问题上和Sun公司的纠纷，微软停止了Visual J++的后续开发。

这之后，作为.Net概念的发起人之一，安德斯·海尔斯伯格被任命为微软.Net的首席架构师，主持.Net的开发工作。