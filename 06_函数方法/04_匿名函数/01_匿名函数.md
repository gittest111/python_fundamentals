# 01_匿名函数

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号：TLV_CN
>
> 服务号：TEDU_TMOOC

---

<!-- TOC -->

- [01_匿名函数](#01)
    - [1. 匿名函数](#1)
        - [1.1. lambda表达式](#11-lambda)
    - [2. 相关视频](#2)

<!-- /TOC -->

## 1. 匿名函数

### 1.1. lambda表达式

lambda表达式又被成为匿名函数，也就是函数并没有具体的名字，语法为lambda:arg1, arg2...argn:expression，一起看一下下面的代码：

```python
def funa(a, b):
    return a + b
```

这个函数如果改成lambda表达式如下：

```python
lambda a, b: a + b
```

lambda表达式本身就是一个没有名字的函数，其中a,b就是这个函数的参数，而冒号后面的表达式就是这个函数的返回值，我们也可以这样用：

```python
g = lambda a, b: a + b
print(g(2, 3))
# 或者可以直接这样写
print((lambda a, b: a + b)(2, 3))

```

通过lambda可以很轻松的构建出一个简单函数

## 2. 相关视频