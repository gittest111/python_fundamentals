def funa(n):
    sum = 0
    for i in range(1, n + 1):
        sum += i
    return sum

a = funa(5)
print(a)
