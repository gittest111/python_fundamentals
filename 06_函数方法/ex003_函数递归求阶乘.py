def funa(n):
    if n == 1:
        return 1
    return n * funa(n - 1)

print(funa(5))
