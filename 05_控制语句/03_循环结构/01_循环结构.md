# 01_循环结构

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号：TLV_CN
>
> 服务号：TEDU_TMOOC

---

<!-- TOC -->

- [1. 概念](#1-概念)
- [2. 循环结构](#2-循环结构)
    - [2.1. while循环](#21-while循环)
    - [2.2. while-else 语句](#22-while-else-语句)
    - [2.3. 死循环](#23-死循环)
    - [2.4. for 循环](#24-for-循环)
    - [2.5. for - else 语句](#25-for---else-语句)
    - [2.6. range函数](#26-range函数)
    - [2.7. 循环嵌套](#27-循环嵌套)
- [3. 相关视频](#3-相关视频)
    - [3.1. while循环](#31-while循环)
    - [3.2. while循环嵌套](#32-while循环嵌套)
    - [3.3. for循环](#33-for循环)
    - [3.4. range函数](#34-range函数)

<!-- /TOC -->

<a id="markdown-1-概念" name="1-概念"></a>
## 1. 概念

循环结构是指在程序中需要反复执行某个功能而设置的一种程序结构，python中循环语句有两种一种是while循环，一种是for循环

<a id="markdown-2-循环结构" name="2-循环结构"></a>
## 2. 循环结构

<a id="markdown-21-while循环" name="21-while循环"></a>
### 2.1. while循环

while循环语法同if的语法相似，如下

```python
while 测试表达式：
    语句块
```

语法说明：while循环在测试表达式为真时会执行内部语句块，之后会再次判断测试表达式是否为真，如果为真则继续执行语句块，如此反复，直到真值测试表达式为假或强制跳出循环。

下面的代码用while循环计算1~10的阶乘即1 * 2 * 3....* 10的值

- [ ] <font color="red">欢迎同学补充下面代码及代码说明</font>

```python

```

代码说明：

<a id="markdown-22-while-else-语句" name="22-while-else-语句"></a>
### 2.2. while-else 语句

语法如下：

```python
while 真值测试：
    语句块1

else:
    语句块2
```

- [ ] <font color="red">欢迎补充语法说明</font>

语法说明：

<a id="markdown-23-死循环" name="23-死循环"></a>
### 2.3. 死循环

死循环是指无法通过自身控制结束的循环，每个while循环都必须有停止运行的途径，这样才不会没完没了的运行下去，例如下面的代码，从1打印到5：

```python
n = 1

while n <= 5:
    print(n)
    n += 1
```

通过不断的给n加1是while的测试表达式 n<=5 最终为False结束while循环，但是如果我们遗漏了n += 1这段代码，这个程序将没完没了的运行下去

```python
n = 1

while n <= 5:
    print(n)
```

由于n始终为1，这个循环将成为死循环，不停打印1，死循环可以通过ctrl+c结束

<a id="markdown-24-for-循环" name="24-for-循环"></a>
### 2.4. for 循环

for循环可以遍历任何序列的项目，如一个列表或者一个字符串

- [ ] <font color=red>欢迎补充语法及说明</font>

语法如下：

```python

```

语法说明：

下面的代码示例为大家展示for循环如何遍历字符串：

- [ ] <font color=red>欢迎同学补充代码</font>

```python

```

<a id="markdown-25-for---else-语句" name="25-for---else-语句"></a>
### 2.5. for - else 语句

语法如下：

```python
for item in 可迭代对象：
    语句块1
else：
    语句块2
```

语法说明：当for循环正常结束后会执行else中的语句块，否则不会执行

<a id="markdown-26-range函数" name="26-range函数"></a>
### 2.6. range函数

rang函数可以生成一个整数序列供for循环迭代

语法：range(start, stop[, step])

说明：

- start参数设定生成整数序列的起始值，默认为0，例如range(10)等效于range(0, 10)
- stop参数设定生成整数序列的结束值，不含该结束值，例如range(1, 5)生成一个包含1~4的整数序列
- step参数为可选参数，默认为1，设定该序列的步进长度，可以为正也可以为负

我们将通过下面的代码示例为大家进行详细说明

```python
# 生成 1 - 100的整数并打印
for i in range(1, 101):
    print(i)

# 生成1 - 10 之间的奇数
for i in range(1, 11, 2):
    print(i) #会分别打印1 3 5 7 9几个数字

# 反向打印1 - 10之间的奇数
for i in range(9, 0, -2):
    print(i) # 会分别打印9 7 5 3 1
```

<a id="markdown-27-循环嵌套" name="27-循环嵌套"></a>
### 2.7. 循环嵌套

在循环内部可以再次嵌套分支结构或循环结构，下面我们通过段代码示例演示：

```python
for i in range(1, 10):
    for j in range(1, i + 1):
        print("%s * %s = %s" % (j, i, j * i), end='  ')
    print()
```

此案例通过for循环嵌套打印九九乘法表，for循环嵌套while循环也是可以的

下面我们将上面打印九九乘法表的案例改成while循环嵌套

```python
i = 1
while i <= 9:
    j = 1
    while j <= i:
        print("%s * %s = %s" % (j, i, j * i), end='  ')
        j += 1
    print()
    i += 1
```

**注意**：循环嵌套时只要内层循环没有结束或者跳出的情况下，程序会一直执行内层循环，不会向下执行，直到内层循环结束或者跳出才会继续向下执行

<a id="markdown-3-相关视频" name="3-相关视频"></a>
## 3. 相关视频

<a id="markdown-31-while循环" name="31-while循环"></a>
### 3.1. while循环

- [爱奇艺](http://www.iqiyi.com/w_19ry0pqsuh.html#vfrm=8-8-0-1)

<a id="markdown-32-while循环嵌套" name="32-while循环嵌套"></a>
### 3.2. while循环嵌套

- [爱奇艺](http://www.iqiyi.com/w_19ry0pqsox.html#vfrm=8-8-0-1)

<a id="markdown-33-for循环" name="33-for循环"></a>
### 3.3. for循环

- [爱奇艺](http://www.iqiyi.com/w_19ry0q5rwp.html#vfrm=8-8-0-1)

<a id="markdown-34-range函数" name="34-range函数"></a>
### 3.4. range函数

- [爱奇艺](http://www.iqiyi.com/w_19ry0q7y6l.html#vfrm=8-8-0-1)