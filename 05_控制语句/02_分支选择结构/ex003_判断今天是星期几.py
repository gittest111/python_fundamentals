#!/usr/bin/env python
# -*- coding:UTF-8 -*-
"""
dongxiaoyong
2018/4/27
ex003_判断今天是星期几.py

使用 if_elif_else 判断今天是星期几
"""
import datetime

# 定义一周七天的列表
weeks = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']

# 获取当前系统时间对应的数字（星期日=0，星期一=1...星期六=6）
index = datetime.datetime.now().weekday()

if index == 0:
    print('今天是', weeks[index])
elif index == 1:
    print('今天是', weeks[index])
elif index == 2:
    print('今天是', weeks[index])
elif index == 3:
    print('今天是', weeks[index])
elif index == 4:
    print('今天是', weeks[index])
elif index == 5:
    print('今天是', weeks[index])
else:
    print('今天是', weeks[6])
