#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
dongxiaoyong
2018/4/9
northwind_helper.py

文件定义了读写 json 文件的方法, 同时将 json 格式的产品信息转换成列表
"""
import json

from tlv00_common_module.northwind.northwind_model import ProductEncoder, Product


def read_from_json(jsonpath):
    """从指定JSON文件中获取产品对象"""
    with open(jsonpath, 'r') as fr:
        return json.loads(fr.read())


def write_to_json(items, jsonpath):
    """将列表写入指定的 json 文件"""
    with open(jsonpath, 'w') as fw:
        fw.write(json.dumps(items, cls=ProductEncoder))


def get_products():
    """将 json 文件中的字典转换成产品对象列表"""
    products = []
    for dic_product in read_from_json('../data/_00_公共模块.json'):
        products.append(Product(
            dic_product.get('ProductId'),
            dic_product.get('ProductName'),
            dic_product.get('SupplierId'),
            dic_product.get('SupplierName'),
            dic_product.get('CategoryId'),
            dic_product.get('CategoryName'),
            dic_product.get('UnitNum'),
            float(dic_product.get('UnitPrice')),
            dic_product.get('StockNum'),
            dic_product.get('OrderNum'),
            dic_product.get('ReOrderNum'),
            dic_product.get('IsAbord')
        ))
    return products
