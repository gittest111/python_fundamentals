# 01_python官网

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. 官网](#1-官网)
    - [1.1. 下载](#11-下载)
    - [1.2. 文档](#12-文档)
    - [1.3. 参考手册](#13-参考手册)
    - [1.4. PYPI](#14-pypi)
    - [1.5. 版本](#15-版本)
        - [1.5.1. V2.7.14](#151-v2714)
        - [1.5.2. v3.6.5](#152-v365)

<!-- /TOC -->

---

<a id="markdown-1-官网" name="1-官网"></a>
## 1. 官网

[https://www.python.org/](https://www.python.org/)

<a id="markdown-11-下载" name="11-下载"></a>
### 1.1. 下载

[https://www.python.org/downloads/](https://www.python.org/downloads/)

<a id="markdown-12-文档" name="12-文档"></a>
### 1.2. 文档

[https://www.python.org/doc/](https://www.python.org/doc/)

<a id="markdown-13-参考手册" name="13-参考手册"></a>
### 1.3. 参考手册

<a id="markdown-14-pypi" name="14-pypi"></a>
### 1.4. PYPI

<a id="markdown-15-版本" name="15-版本"></a>
### 1.5. 版本

<a id="markdown-151-v2714" name="151-v2714"></a>
#### 1.5.1. V2.7.14

该版本至 2020 年停止支持

目前已有越来越多的厂商宣布取消对 Python2.X的支持

<a id="markdown-152-v365" name="152-v365"></a>
#### 1.5.2. v3.6.5
